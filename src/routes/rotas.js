const router = require("express").Router();
const dbController = require("../controller/dbController");

//rotas para consultas
router.get("/getTables/", dbController.getTables);
router.get("/getDescTable/", dbController.getDescTable)

module.exports = router;